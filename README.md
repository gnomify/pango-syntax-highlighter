Forked from https://github.com/LinuxJedi/pango-syntax-highlighter

# Install

```
pip install pygments -t site-packages
```

Usage example:

```
pangopygments.py --lexer cpp myfile.cpp
```
