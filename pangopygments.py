#!/usr/bin/python
# -*- coding:utf8 -*-
'''
A pygments formatter for Pango text markup.

Written by Soheil Hasss Yeganeh, 2014.
Not copyrighted, in public domain.
'''

import sys
import os
import codecs
import argparse

sys.path.append(os.path.join(os.path.dirname(__file__), 'site-packages'))

import pygments
from pygments import lexers
from pygments.formatter import Formatter

class PangoFormatter(Formatter):
  ''' Based on the HTML 3.2 formatter in pygments:
      http://pygments.org/docs/formatterdevelopment/ '''
  def __init__(self, **options):
    Formatter.__init__(self, **options)

    self.styles = {}

    for token, style in self.style:
      start_tag = close_tag = ''

      if style['color']:
        start_tag += '<span fgcolor="#%s">' % style['color']
        close_tag = '</span>' + close_tag

      if style['bold']:
        start_tag += '<b>'
        close_tag = '</b>' + close_tag

      if style['italic']:
        start_tag += '<i>'
        close_tag = '</i>' + close_tag

      if style['underline']:
        start_tag += '<u>'
        close_tag = '</u>' + close_tag

      self.styles[token] = (start_tag, close_tag)

  def format(self, tokensource, outfile):
    lastval = ''
    lasttype = None

    for ttype, value in tokensource:
      while ttype not in self.styles:
        ttype = ttype.parent

      if ttype == lasttype:
        lastval += value
      else:
        if lastval:
          stylebegin, styleend = self.styles[lasttype]
          outfile.write(stylebegin + lastval + styleend)

        lastval = value
        lasttype = ttype

    if lastval:
      stylebegin, styleend = self.styles[lasttype]
      outfile.write(stylebegin + lastval + styleend)

def highlight(snippet, lexer):
  ''' snippet is the string of code snippets, and lang, the language name. '''
  # The highlighter highlights (i.e., adds tags around) operators
  # (& and ;, here), so let's use a non-highlighted keyword, and escape them
  # after highlighting.
  snippet = snippet.replace("&", "__AMP__").replace("<", "__LT__")

  # Pygments messes up initial and final newlines; fix up
  begin = ''
  if snippet[:1] == '\n':
	  begin = '\n'
	  snippet = snippet[1:]
  end = ''
  if snippet[-1:] == '\n':
	  end = '\n'
	  snippet = snippet[:-1]

  snippet = pygments.highlight(snippet, lexer, PangoFormatter())

  if snippet[0] == '\n' and begin == '\n':
	  begin = ''
  if snippet[-1] == '\n' and end == '\n':
	  end = ''

  snippet = snippet.replace("__AMP__", "&amp;").replace("__LT__", "&lt;").replace("\\", "\\\\")

  return begin + snippet + end

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='pangopygments')
  parser.add_argument('-l', '--lexer', type=str, help='Explictly specify lexer to use')
  parser.add_argument('file', nargs='*')
  args = parser.parse_args()

  if len(args.file) != 1:
    print(parser.print_help(sys.stderr))
    parser.exit()
    sys.exit(1)

  input = args.file[0]

  lexer = None
  if args.lexer is not None:
    lexer = pygments.lexers.get_lexer_by_name(args.lexer)
    print("Lexer name: " + lexer.name, file=sys.stderr)

  # Guess - Slow
  if lexer is None:
    lexer = pygments.lexers.get_lexer_for_filename(input)
    print("Lexer name: " + lexer.name, file=sys.stderr)

  if lexer is None:
    print("Lexer not found for " + input, file=sys.stderr)
    sys.exit(1)

  print(highlight(open(input).read(), lexer))
